function otraFuncion() {
  serompe();
}

function serompe() {
  return 3 + z;
}

function serompeAsync(cb) {
  setTimeout(() => {
    try {
      return 3 + z;
    } catch (err) {
      console.error('Error en mi funcion asincrona');
      cb(err);
    }
  }, 1000);
}

try {
  // otraFuncion();
  serompeAsync(function (err) {
    console.log(err.message);
  });
} catch (err) {
  console.error('Vaya, algo se ha roto...');
  console.error(err);
  console.log('Pero no pasa nada, lo hemos capturado');
}

console.log('Esto esta al final');
